import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ForecastComponent } from './forecast/forecast.component';
import { GetForecastService } from './services/get-forecast.service';
import { TemperaturePipe } from './forecast/temperature.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ForecastComponent,
    TemperaturePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [GetForecastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
