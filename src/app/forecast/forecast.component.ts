import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import {Forecast} from './forecast.model';
import {GetForecastService} from '../services/get-forecast.service'

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html'
})
export class ForecastComponent implements OnInit {

  forecasts: Forecast[];

  constructor(private getForecastService: GetForecastService) { }

  ngOnInit() {
    this.loadForecast()
  }

loadForecast(){
  this.getForecastService.getForecast()
      .subscribe(
        owForecasts => this.forecasts = owForecasts, 
        err => {
          console.log(err);
        }
      );
}

}
