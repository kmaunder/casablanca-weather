export class Forecast{
    public temp: number;
    public date: string;
    public icon: string;    
    
    constructor(temp: number, date: string, icon: string){
        this.temp = temp;
        this.date = date;
        this.icon = icon;
    }
}