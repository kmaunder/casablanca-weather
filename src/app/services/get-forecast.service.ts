import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {Forecast} from '../forecast/forecast.model';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class GetForecastService {

  constructor(private http: Http) { }

  private forecastApi = 'http://api.openweathermap.org/data/2.5/forecast?id=3130763&units=metric&appid=b0f047c1ba4907b4be5e5cb0b3960fe6';

  getForecast() : Observable<Forecast[]>{
    return this.http.get(this.forecastApi)
      .map((res: Response) => res.json().list)

      .catch((error: any) => Observable.throw(error.json().error || 'API Error'));
  }

}
