# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a single HTML page displaying the 5 day weather forecast for Casablanca

### How do I get set up? ###

This is an Angular 2 application using Typescript and SASS, running on the latest version of Node.js (v6.10.3)

Use the cli to create a new Angular 2 project with SASS: 
ng new casablanca-weather --style=sass

Get the 'src' files from bitbucket and update the new project.
In particular ensure you have added these folders within src\app:
1. forecast
2. service
3. styles

Ensure you have updated these files:
1. styles.sass 
2. app.component.html 
2. app.module.ts

Use the cli to run and test the app locally on http://localhost:4200/ with:
ng serve

Use the cli to build the app with aot with:
ng build --prod --oat

This will create a dist folder, the contents of which you can upload on to your application server

### Enhancements ###

With more time I would be able to make significant improvements to the user experience.
1. The date/time format would be more user friendly. I used a date pipe for this with arguments (date:'EE MM/dd HH:mm') however this failed to work on iOS devices
2. The current format works fairly well for the next 24 hours, however beyond that each day should be grouped together and depending on the audience requirements less or more information should be shown by day
3. With the current build an API call is made every page load, which is not optimal and a call would only need to be made every 3 hours at most
4. Casablanca is hard coded!